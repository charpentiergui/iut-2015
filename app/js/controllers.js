'use strict';

/* Controllers */

var moduleCtrl = angular.module('myApp.controllers', []);

moduleCtrl.controller('tableCtrl', ['$scope', function ($scope) {
    $scope.items = [
        {
            "id" : "0",
            "lastname" : "Hevery",
            "firstname" : "Miško",
            "fct" : "fondateur"
        },
        {
            "id" : "1",
            "lastname" : "Abronsw",
            "firstname" : "Adam",
            "fct" : "fondateur"
        },
        {
            "id" : "2",
            "lastname" : "Minar",
            "firstname" : "Igor",
            "fct" : "Developpeur"
        },
        {
            "id" : "3",
            "lastname" : "Ford",
            "firstname" : "Brian",
            "fct" : "Developpeur"
        }];

    $scope.user={};

    $scope.add = function(){
        //on utilise la variable $scope.user et on l'ajoute au tableau $scope.items
        // !! ne pas oublier de vider $scope.user
    };

    $scope.delete = function(item){
        //on utilise l'identifiant de l'objet passé en paramètre et on le supprime du tableau $scope.items
        // array.splice(début, nbASupprimer[, élem1[, élem2[, ...]]]) https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/splice
    };

}]);